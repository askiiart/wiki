*[fstab]: The file that lists what to mount on boot, and where to mount it.
*[snapraid.conf]: The SnapRAID config file
*[mergerfs]: Combines multiple directories into one - see the mergerfs docs or Perfect Media Server wiki for more details.
*[snapraid]: A parity-based backup solution - see the SnapRAID docs or Perfect Media Server wiki for more details
*[SnapRAID]: A parity-based backup solution - see the SnapRAID docs or Perfect Media Server wiki for more details
*[PMS]: Perfect Media Server
*[Plex]: A proprietary media server software
*[Jellyfin]: A FOSS media server software
*[Docker]: A popular containerization platform
*[Podman]: A FOSS drop-in replacement for Docker
*[SSH]: Secure Shell - a protocol for secure remote shell access