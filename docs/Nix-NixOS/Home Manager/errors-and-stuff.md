# Errors and stuff

## Security errors

Home Manager will warn about insecure packages, to bypass one-time you can run this:

```sh
export NIXPKGS_ALLOW_INSECURE=1
```

## Could not find suitable profile directory

Run this:

```sh
nix-env -q
```

## file 'home-manager/home-manager/home-manager.nix' was not found in the Nix search path

Error:

```txt
error: file 'home-manager/home-manager/home-manager.nix' was not found in the Nix search path (add it using $NIX_PATH or -I)

       at «none»:0: (source not available)
```

idk :(

Sometimes restarting fixes it temporarily.
