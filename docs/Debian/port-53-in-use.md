# Port in use (connman)

## Problem

When trying to run DNS thing, `connman` is already using port 53.

```bash
docker compose up --detach --build --remove-orphans
[+] Running 0/1
 ⠿ Container pihole  Starting                                                       0.2s
Error response from daemon: driver failed programming external connectivity on endpoint pihole (bc535387671f0d471f11f8ade5eedc4771126c057e2099931e8ef49461111149): Error starting userland proxy: listen tcp4 0.0.0.0:53: bind: address already in use
```

## Solution

- Find the `connman.service` file:

```bash
grep -Ril "connman" /etc/systemd/
```

- Add `--nodnsproxy` to the `ExecStart` line:

```bash
ExecStart=/usr/sbin/connmand -n --nodnsproxy
```

- Reload and restart stuff:

```bash
systemctl daemon-reload
systemctl restart connman.service
```

- Try your thing again. For example:

```bash
docker compose up -d --remove-orphans
```
