# Port is privileged, cannot be used

When running a container that uses a priviledged port, AKA anything less than 1024, you will get the following error:

```text
Error starting userland proxy: error while calling PortManager.AddPort(): cannot expose privileged port 80, you can add 'net.ipv4.ip_unprivileged_port_start=80' to /etc/sysctl.conf
```

To fix this, you need to add the following to your `/etc/sysctl.conf` file:

```conf
net.ipv4.ip_unprivileged_port_start=0
```

Then, you can just run the container again.
