# Find Fastest Mirror

You can find the fastest apt mirror using `netselect-apt`. Install it with:

```bash
sudo apt install netselect-apt
```

Then run it:

```bash
sudo netselect-apt -c US -a amd64 -n stable
```

You should get something like this:

```text
Running netselect to choose 10 out of 33 addresses.     
..............................................................................................................................................................................................................................
The fastest 10 servers seem to be:

	http://mirror.dal.nexril.net/debian/
	http://mirror.us.oneandone.net/debian/
	http://mirrors.gigenet.com/debian/
	http://mirror.steadfast.net/debian/
	http://mirrors.xtom.com/debian/
	http://la.mirrors.clouvider.net/debian/
	http://mirror.keystealth.org/debian/
	http://mirror.clarkson.edu/debian/
	http://ftp.us.debian.org/debian/
	http://mirror.cogentco.com/debian/

Of the hosts tested we choose the fastest valid for http:
        http://mirror.dal.nexril.net/debian/

Writing sources.list.
sources.list exists, moving to sources.list.1672257815
Done.
```
