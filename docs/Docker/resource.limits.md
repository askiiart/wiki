# Resource limits

You can limit the amount of CPU and/or memory resources that a container can use.

## CPU

| Option | Description |
|--------|-------------|
| `--cpus` | Set number of CPUs thee container can use |
| `--cpu-period` | Limits the length of time it can schedule the CPU before being throttled (used alongside `--cpu-quota`) |
| `--cpu-quota` | The throttling setting activated when the CPU is scheduled longer than `--cpu-period` |
| `--cpuset-cpus` | Limit the container to specific CPUs or cores (e.g. 0-3, 0,1) |
| `--cpu-shares` | The number of relative shares of the CPU the container can use (default 1024) |

## Memory

A markdown table:

| Option | Description |
|--------|-------------|
| `-m` or `--memory` | Memory limit (minimum 6m (megabytes)) |
| `--memory-swap` | How much swap is available - [details](https://docs.docker.com/config/containers/resource_constraints/#--memory-swap-details) |
| `--memory-swappiness` | "By default, the host kernel can swap out a percentage of anonymous pages used by a container. You can set --memory-swappiness to a value between 0 and 100, to tune this percentage" - [details]() |
| `--memory-reservation` | Soft limit less than `--memory` (for when there is low memory on host), *soft* limit, so usage may exceed this. |
| `--oom-kill-disable` | Disable OOM Killer (stops from killing container processes when out-of-memory error occurs) - Make sure to use `-m`, or host processes could be killed |

## Example

Note: `--xxxx 4` in `docker run` would be replaced with `xxxx` in `docker-compose.yml`. See below:

```yml
service:
  image: nginx
  mem_limit: 512m
  mem_reservation: 128M
  cpus: 0.5
  ports:
    - "80:80"
```

## See also

- [Docker documentation](https://docs.docker.com/config/containers/resource_constraints/)
- [Baeldung docs](https://www.baeldung.com/ops/docker-memory-limit) (includes `docker compose` examples)
