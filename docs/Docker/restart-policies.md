# Restart Policies

Restart policies control whether and how Docker attempts to restart a container.

| option | description |
| ------ | --- |
| no | does not restart automatically |
| on-failure[:max-retry] | restarts only when the container exits with a non-zero exit code, and when it has been restarted fewer than max-retry times. |
| always | always restarts the container if it stops. If it is manually stopped, it is restarted only when Docker daemon restarts or the container itself is manually restarted. |
| unless-stopped - always restarts the container unless it is manually stopped. Does not restart when the docker daemon is restarted. |

If no restart policy is provided, the default is no.

## Example

```yaml
version: '3.3'
services:
    simple-torrent:
        ports:
            - '3000:3000'
        volumes:
            - '/path/to/my/downloads:/downloads'
            - '/path/to/my/torrents:/torrents'
        image: boypt/cloud-torrent
        restart: unless-stopped
```
