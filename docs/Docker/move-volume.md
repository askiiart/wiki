# Move Docker program data

Docker stores all its data in `/var/lib/docker` by default. This is usually fine, but this directory grows quickly, so we'll move it to `/mnt/big-stuff/docker-program-data/`

Edit `/lib/systemd/system/docker.service` and add the `--data-root` option to the ExecStart line:

```sh
ExecStart=/usr/bin/dockerd --data-root /mnt/big-stuff/docker-program-data/ -H fd:// $DOCKER_OPTS
```

If you've already done stuff with docker, you'll need to move the data:

```sh
sudo systemctl stop docker
sudo mv /var/lib/docker/ /mnt/big-stuff/docker-program-data/
sudo systemctl daemon-reload
sudo systemctl start docker
```

You may also need to set up some symlinks:

```bash
ln -s source_file link_file
```
