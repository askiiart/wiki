# Fix APIPA (veth)

## Problem

On Debian 11, when:

1. Using docker containers that use the `host` network mode.
2. `PreferredTechnologies` is set to `ethernet,[...]` in `/etc/connman/main.conf`.
   - This may not be a problem when ethernet is plugged in, not just wifi.

The system **uses a veth interface** to connect to the internet, which uses an APIPA (169.254.*.*) IP address, so the system can only contact devices on the LAN.

## Solution

Edit `/etc/connman/main.conf` and uncomment the line `# NetworkInterfaceBlacklist = vmnet,vboxnet,virbr,ifb,ve-,vb-`

Result:

```conf
NetworkInterfaceBlacklist = vmnet,vboxnet,virbr,ifb,ve-,vb-
```

## References

- [A tale of Docker and Linux ConnMan](https://sitaram.substack.com/p/a-tale-of-docker-and-linux-connman)
- [Arch Linux Docs](https://wiki.archlinux.org/title/ConnMan#Blacklist_interfaces)
