# Drives and ZFS and stuff

`/etc/fstab`:

```txt
# <file system> <mount point> <type> <options> <dump> <pass>
/dev/pve/root / ext4 errors=remount-ro 0 1
UUID=2755-AA1E /boot/efi vfat defaults 0 1
/dev/pve/swap none swap sw 0 0
proc /proc proc defaults 0 0

# note that zfs mounting is handled by zfs
UUID=c7283793-37c6-4b01-95e1-cca26af0da8f                  /mnt/cache   btrfs         defaults 0 0

PARTUUID=3857f198-72c5-644a-9da6-1e542d370785              /mnt/media0  xfs           defaults 0 0
PARTUUID=1674ea7b-55d1-2c4f-bb5e-75142feae5db              /mnt/media1  xfs           defaults 0 0
PARTUUID=abec3d4f-baab-a94d-a9b4-2b64cb8f4de9              /mnt/media2  xfs           defaults 0 0
PARTUUID=d7196edd-9870-f14d-8269-387338e79939              /mnt/media3  xfs           defaults 0 0
/mnt/media0:/mnt/media1:/mnt/media2:/mnt/media3            /mnt/media   fuse.mergerfs defaults,allow_other,use_ino,category.create=mfs,moveonenospc=true,minfreespace=4G,cache.files=partial,dropcacheonclose=true  0 0
```

## Partitions

```txt
root@askiiart:~# lsblk
NAME                         MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda                            8:0    0   3.6T  0 disk 
├─sda1                         8:1    0     2T  0 part 
└─sda2                         8:2    0   1.6T  0 part 
sdb                            8:16   0 119.2G  0 disk 
├─sdb1                         8:17   0  1007K  0 part 
├─sdb2                         8:18   0     1G  0 part /boot/efi
└─sdb3                         8:19   0 118.2G  0 part 
  ├─pve-swap                 252:0    0     8G  0 lvm  [SWAP]
  ├─pve-root                 252:1    0  54.3G  0 lvm  /
  ├─pve-data_tmeta           252:2    0     1G  0 lvm  
  │ └─pve-data-tpool         252:4    0  53.9G  0 lvm  
  │   ├─pve-data             252:5    0  53.9G  1 lvm  
  │   └─pve-vm--100--disk--0 252:6    0     4G  0 lvm  
  └─pve-data_tdata           252:3    0  53.9G  0 lvm  
    └─pve-data-tpool         252:4    0  53.9G  0 lvm  
      ├─pve-data             252:5    0  53.9G  1 lvm  
      └─pve-vm--100--disk--0 252:6    0     4G  0 lvm  
sdc                            8:32   0   3.6T  0 disk 
├─sdc1                         8:33   0     2T  0 part 
└─sdc2                         8:34   0   1.6T  0 part 
sdd                            8:48   0  12.7T  0 disk 
├─sdd1                         8:49   0     2T  0 part 
└─sdd2                         8:50   0  10.7T  0 part 
sde                            8:64   1   7.3T  0 disk 
└─sde1                         8:65   1   7.3T  0 part /mnt/parity
sdf                            8:80   1 476.9G  0 disk 
└─sdf1                         8:81   1 476.9G  0 part /mnt/cache
root@askiiart:~# blkid
/dev/mapper/pve-root: UUID="81f429ab-6ef1-4bef-a46b-74249c2de7c7" BLOCK_SIZE="4096" TYPE="ext4"
/dev/sdf1: UUID="c7283793-37c6-4b01-95e1-cca26af0da8f" UUID_SUB="404f1350-a146-474a-9c2d-1b73669459de" BLOCK_SIZE="4096" TYPE="btrfs"
/dev/sdb2: UUID="2755-AA1E" BLOCK_SIZE="512" TYPE="vfat" PARTUUID="e00d12e5-5744-4876-8240-08420322ff18"
/dev/sdb3: UUID="pazYIV-u3iI-OIAE-sCRo-CNc8-HP47-oUN6A9" TYPE="LVM2_member" PARTUUID="4c795763-e15b-4d18-9c5f-4ef95d42971f"
/dev/mapper/pve-swap: UUID="dd063ef6-f4b6-41e9-9668-bfc63adc65b6" TYPE="swap"
/dev/sde1: UUID="8d8cb485-eb23-475b-a9c1-03b8309cdbaa" BLOCK_SIZE="4096" TYPE="xfs" PARTUUID="d7196edd-9870-f14d-8269-387338e79939"
/dev/sdd2: UUID="bab6389d-feb1-41e7-b07f-93080243eb48" BLOCK_SIZE="4096" TYPE="xfs" PARTUUID="3857f198-72c5-644a-9da6-1e542d370785"
/dev/sdd1: LABEL="storage" UUID="16414527426920089236" UUID_SUB="1719498991484903801" BLOCK_SIZE="4096" TYPE="zfs_member" PARTUUID="8742ec6c-c30d-f84b-8a23-90ec24b00c9a"
/dev/sdb1: PARTUUID="8f8094b6-b73f-4724-97e7-eece6c5448ff"
/dev/sdc2: UUID="4a7e1c36-05f9-4345-80da-9c177376cd00" BLOCK_SIZE="4096" TYPE="xfs" PARTUUID="abec3d4f-baab-a94d-a9b4-2b64cb8f4de9"
/dev/sdc1: LABEL="storage" UUID="16414527426920089236" UUID_SUB="4034933404348229319" BLOCK_SIZE="4096" TYPE="zfs_member" PARTUUID="685ab334-2e81-3c4f-8a51-76a988ba8f1c"
/dev/sda2: UUID="94e6e44d-dcc7-40cf-a0b8-cea939613d0f" BLOCK_SIZE="4096" TYPE="xfs" PARTUUID="1674ea7b-55d1-2c4f-bb5e-75142feae5db"
/dev/sda1: LABEL="storage" UUID="16414527426920089236" UUID_SUB="10156536812731289767" BLOCK_SIZE="4096" TYPE="zfs_member" PARTUUID="aaae4977-a1fd-3f44-bb89-4ec368bfc906"
```

### Drive 1

- Capacity: 14 TB
- Model: Exos 2X14, ST14000NM0121
- SN: ZKL2Q58G
- Bought 2024-02-10-ish

Partitions:

| PARTUUID                               | Size    | Filesystem | Purpose                   |
| -------------------------------------- | ------- | ---------- | ------------------------- |
| `8742ec6c-c30d-f84b-8a23-90ec24b00c9a` | 2 TB    | ZFS        | Redundant ZFS storage     |
| `3857f198-72c5-644a-9da6-1e542d370785` | 10.7 TB | XFS        | Media storage w/ MergerFS |

### Drive 2

- Capacity: 4 TB
- Model: Toshiba X300

Partitions:

| PARTUUID                               | Size   | Filesystem | Purpose                   |
| -------------------------------------- | ------ | ---------- | ------------------------- |
| `1674ea7b-55d1-2c4f-bb5e-75142feae5db` | 2 TB   | ZFS        | Redundant ZFS storage     |
| `3857f198-72c5-644a-9da6-1e542d370785` | 1.6 TB | XFS        | Media storage w/ MergerFS |

### Drive 3

- Capacity: 4 TB
- Model: Toshiba X300

Partitions:

| PARTUUID                               | Size   | Filesystem | Purpose                   |
| -------------------------------------- | ------ | ---------- | ------------------------- |
| `685ab334-2e81-3c4f-8a51-76a988ba8f1c` | 2 TB   | ZFS        | Redundant ZFS storage     |
| `abec3d4f-baab-a94d-a9b4-2b64cb8f4de9` | 1.6 TB | XFS        | Media storage w/ MergerFS |

### Drive 4

- Capacity: 8TB

Partitions:

| PARTUUID                               | Size   | Filesystem | Purpose                   |
| -------------------------------------- | ------ | ---------- | ------------------------- |
| `d7196edd-9870-f14d-8269-387338e79939` | 7.3 TB | XFS        | Media storage w/ MergerFS |
