# ZFS stuff

**In case of drive failure, go [here](#restore-after-drive-failure)!**

## Initial setup

Create partition table and partitions on drives:

```sh
fdisk /dev/sd{$LETTER}
```

```txt
g
# create 4g partition (ZFS)
n
\n
\n
+2T
# create 12g partition (mergerfs (for media))
n
\n
\n
\n
# write
w
```

format mergerfs/media partitions:

```sh
mkfs.xfs /dev/sd{$LETTER}2
```

---

Set up ZFS pool:

```sh
zpool create storage raidz1 /dev/sdb1 /dev/sdc1 /dev/sdd1
```

set mountpoint:

```sh
zfs set mountpoint=/mnt/user storage
```

## Restore after drive failure

```sh
zpool replace storage $NEW_ZFS_PARTITION
zpool status storage
```
