# Install Firmware

## During Linux Installation

When installing Linux, you may get an error like this:

```
Some of your hardware needs non-free firmware files to operate. The firmware can be loaded from removable media, such as a USB stick or floppy.

The missing firmware files are: iwlwifi-3168-29.ucode iwlwifi-3168-28.ucode iwlwifi-3168-27.ucode iwlwifi-3168-26.ucode iwlwifi-3168-25.ucode iwlwifi-3168-24.ucode iwlwifi-3168-23.ucode iwlwifi-3168-22.ucode

If you have such media available now, insert it, and continue.

Load missing firmware from removable media?
[ ] No
[x] Yes
```

These are all actually just different versions of the one firmware file you need. Just get `iwlwifi-3168-29.ucode` from [here](iwlwifi-3168-29.ucode) or [here](/static/iwlwifi-3168-29.ucode), put it on another USB drive, then insert it and continue.

## Post-Installation

Get the firmware from [here](iwlwifi-3168-29.ucode), then move it to `/lib/firmware`, and restart.

```bash
sudo mv iwlwifi-3168-29.ucode /lib/firmware/
reboot
```