# Spice Guest Tools

These are the software packages for the guest OS that provide stuff like copy-paste, shared folders, graphics drivers, etc. They're not required, but are quite useful (and tiny!)

## Windows

The download is [here](https://www.spice-space.org/download/windows/spice-guest-tools/spice-guest-tools-latest.exe), it's easy. Just install it and restart.

## Linux

Linux guests use the packages `spice-vdagent` (for copy-paste, shared folders, etc.) and `xf86-video-qxl` (for graphics drivers) (or `xf86-video-qxl-devel` if you're building from source, according to GitHub copilot, IDK). The `spice-vdagent` package is available in most distributions' repositories, but the `xf86-video-qxl` package is not. You can build it from source (download [here](https://www.spice-space.org/download.html)).

## macOS

No. Maybe the Linux ones would work, IDK. I don't feel like testing it. Check out [this](https://github.com/utmapp/UTM/discussions/3772) and [this](https://docs.getutm.app/).
