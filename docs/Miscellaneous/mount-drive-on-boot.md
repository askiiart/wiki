# Mount Drive On Boot

To mount a drive on boot, first find the UUID of the drive:

```bash
sudo blkid
```

Then add the following to `/etc/fstab`:

```conf
UUID=<UUID_OF_DRIVE> /mnt/big-stuff  ext4    defaults
```

For example, for my big drive (4TB Toshiba X300 Performance):

```conf
# big-stuff
UUID=d68f1a75-af20-4627-8382-7198c3a34b5d /mnt/big-stuff  ext4    defaults
```
