# SSH Port Forwarding

## Local port forwarding

This sends the traffic to the host you're SSHing into, e.g. accessing a service on a remote server.

```sh
ssh -L <local-port>:localhost:<remote-port> <user>@<server>
```

Or to forward that port to a different host *through* the SSH server:

```sh
ssh -L localhost:<local-port>:<different-host>:<remote-port> <user>@<server>
```

## Remote port forwarding

This send the traffic from the SSH server to your local machine, e.g. running a web server on your local machine and making it accessible from the server.

```sh
ssh -R <remote-port>:localhost:<local-port> <user>@<server>
```
