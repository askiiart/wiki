# *arr not redownloading after deletion

Note: it feels weird to refer to "*arr" constantly, so I'm just gonna refer to Radarr for now, but everything applies to Radarr, Sonarr and Lidarr.

Radarr has a failsafe where in case the folder of a library is completely empty, it won't scan the disk for anything. This is presumably meant to be in case, for example, you accidentally mount the volume at the wrong location, but it also means it won't do anything if all your media is deleted.

To fix this, just create an empty file in each library using `touch`, then run update all.

If it's still not working, make sure your permissions are correct and that "Create empty folders" is enabled under the advanced options in Settings -> Media Management -> Folders.
